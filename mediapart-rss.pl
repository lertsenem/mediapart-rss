#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::DOM;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(decode encode);
use XML::RSS;
use Data::Dumper;

my $login = decode('UTF-8', $ENV{MEDIAPART_LOGIN});
my $pwd   = decode('UTF-8', $ENV{MEDIAPART_PWD});
my $file  = $ENV{MEDIAPART_RSSFILE};

die 'Please set MEDIAPART_LOGIN, MEDIAPART_PWD and MEDIAPART_RSSFILE. Aborting.' unless ($login && $pwd && $file);

my $ua   = Mojo::UserAgent->new();
   $ua->max_redirects(4);
my $feed = XML::RSS->new();

# Get Feed
$feed->parse($ua->get('https://www.mediapart.fr/articles/feed')->result->body);

# Connection
my $args = {
    name     => $login,
    password => $pwd,
    op       => 'Se+connecter'
};
$ua->post('https://www.mediapart.fr/login_check' => form => $args);

# Get articles
foreach my $item (@{$feed->{'items'}}) {
    my $link        = Mojo::URL->new($item->{link});
    my $description = sprintf('<p><b>%s</b></p>', $item->{description});


    my $page = decode('UTF-8', $ua->get($link)->result->body);
    if ($page =~ "s/Identifiez-vous/gm") {
        $ua->post('https://www.mediapart.fr/login_check' => form => $args);
        $page = decode('UTF-8', $ua->get($link)->result->body);
    }
    my $dom  = Mojo::DOM->new($page);
    $dom->find('span[aria-hidden="true"] span.dropcap')->each(sub {
        my ($e, $num) = @_;
        $e->parent->remove;
    });
    $dom->find('.news__body__center__article')->each(sub {
        my ($e, $num) = @_;
        $description .= $e->content;
    });
    $item->{description} = $description if $description;
}
$feed->save($file);
