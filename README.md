# Générateur de flux RSS non tronqué de Mediapart

Le flux RSS de Mediapart est modifié pour contenir les articles en entier et non de manière tronquée.

La modification du flux nécessite un compte et un [abonnement valide](https://www.mediapart.fr/abo/abonnement/normal).

## Dépendances

```
apt install libmojolicious-perl libxml-rss-perl
```

## Configuration

La configuration est gérée par trois variables d’environnement :
- `MEDIAPART_RSSFILE` : chemin du fichier RSS à créer
- `MEDIAPART_LOGIN` : l’email ou votre identifiant utilisé pour votre compte sur Mediapart
- `MEDIAPART_PWD` : le mot de passe de votre compte sur Mediapart

## Installation

```
wget https://framagit.org/fiat-tux/rss/mediapart-rss/-/raw/master/mediapart-rss.pl -O /opt/mediapart-rss.pl
chmod +x /opt/mediapart-rss.pl
```

## Utilisation

```
MEDIAPART_RSSFILE="/var/www/mon_flux_rss_des_articles_mediapart.rss" MEDIAPART_LOGIN="me@exemple.org" MEDIAPART_PWD="foobarbaz" /opt/mediapart-rss.pl
```

## Cron

Voici un exemple de tâche cron pour mettre à jour le flux toutes les heures :
```
45 * * * * MEDIAPART_RSSFILE="/var/www/mon_flux_rss_des_articles_mediapart.rss" MEDIAPART_LOGIN="me@exemple.org" MEDIAPART_PWD="foobarbaz" /opt/mediapart-rss.pl
```

## Licence

Affero GPLv3. Voir le fichier [LICENSE](LICENSE).
